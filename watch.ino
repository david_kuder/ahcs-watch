#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <LiquidCrystal.h>

// Base Code is 3254 / 60
#define SUPPORT_AMPM      // 236 / 1
#define SUPPORT_DATE      // 154 / 0
#define SUPPORT_LONGDATE  // 312 / 1
#define SUPPORT_ALARM     // 414-472 / 4
#define SUPPORT_SNOOZE
#define SUPPORT_SLEEP     // 214-280 / 5
#define SUPPORT_WATCHDOG  // 64 / 0
#define SUPPORT_TILT

#ifdef SUPPORT_SLEEP
volatile bool buttonWake;
uint8_t mcucr1, mcucr2;
#endif

uint8_t Hour, Min, Sec;
uint8_t Date, Month, DayofWeek, Year;

#ifdef SUPPORT_AMPM
bool DisplayAMPM;
#else
#define DisplayAMPM 0
#endif

#ifdef SUPPORT_LONGDATE
bool DisplayLongDate;
#else
#define DisplayLongDate 0
#endif

#ifdef SUPPORT_ALARM
bool Alarming;
bool Snoozing;
uint8_t AlarmH, AlarmM, AlarmD;
#endif

bool Backlight;

#define PIN_TILT       0
#define PIN_CLOCK_SS   1
#define PIN_CLOCK_SCK  2
#define PIN_CLOCK_DAT  3

#define PIN_LCD_D4     4
#define PIN_LCD_D5     5
#define PIN_LCD_D6     6
#define PIN_LCD_D7     7
#define PIN_LCD_RS    12
#define PIN_LCD_EN    13

#define PIN_BUTTON0    8
#define PIN_BUTTON1    9
#define PIN_SPEAKER   10
#define PIN_BACKLIGHT 11
#define PIN_MAINPOWER 14
#define PIN_RESET     15

LiquidCrystal lcd(PIN_LCD_RS, PIN_LCD_EN, PIN_LCD_D4, PIN_LCD_D5, PIN_LCD_D6, PIN_LCD_D7);

void setMainPower(bool val) {
  //pinMode(PIN_MAINPOWER, OUTPUT);
  DDRB |= (1 << 6);
  //digitalWrite(PIN_MAINPOWER, val);
  PORTB = (PORTB & ~(1<<6)) | (val << 6);
}

void setBacklight(bool val) {
  //pinMode(PIN_BACKLIGHT, OUTPUT);
  DDRB |= (1 << 3);
  //digitalWrite(PIN_BACKLIGHT, val);
  PORTB = (PORTB & ~(1<<3)) | (val << 3);
  Backlight = val;
}

void setSpeaker(bool val) {
  //pinMode(PIN_SPEAKER, OUTPUT);
  DDRB |= (1 << 2);
  //digitalWrite(PIN_SPEAKER, val);
  PORTB = (PORTB & ~(1<<2)) | (val << 2);
}

void wakeTasks() {
  setMainPower(LOW); // Enable +5V
  setBacklight(LOW); // Disable Backlight
  setSpeaker(LOW);

  // Clock BUS
  //pinMode(PIN_CLOCK_SS, OUTPUT);
  //digitalWrite(PIN_CLOCK_SS, LOW);
  //pinMode(PIN_CLOCK_SCK, OUTPUT);
  //digitalWrite(PIN_CLOCK_SCK, LOW);
  DDRA |= 0x06;
  PORTA &= ~(0x06);

  // LCD BUS
  //pinMode(PIN_LCD_EN, OUTPUT);
  //digitalWrite(PIN_LCD_EN, LOW);
  //pinMode(PIN_LCD_RS, OUTPUT);
  DDRB |= 0x30;
  PORTB |= 0x10;
  //pinMode(PIN_LCD_D4, OUTPUT);
  //pinMode(PIN_LCD_D5, OUTPUT);
  //pinMode(PIN_LCD_D6, OUTPUT);
  //pinMode(PIN_LCD_D7, OUTPUT);
  DDRA |= 0xf0;

  // setup buttons
  //pinMode(PIN_BUTTON0,INPUT);
  //pinMode(PIN_BUTTON1,INPUT);
  DDRB &= ~(0x03);

  // setup the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // Write-Enable the Clock
  writeClockByte(0x8E, 0x00);

  // Read Config from Clock EEPROM
#ifdef SUPPORT_AMPM
  DisplayAMPM = readClockByte(0xC1);
#endif
#ifdef SUPPORT_LONGDATE
  DisplayLongDate = readClockByte(0xC3);
#endif

#ifdef SUPPORT_ALARM
  Snoozing = readClockByte(0xCB);
  if(!Snoozing) {
    AlarmH = makeDEC(readClockByte(0xC5));
    AlarmM = makeDEC(readClockByte(0xC7));
    AlarmD = makeDEC(readClockByte(0xC9));
  } else {
    AlarmH = makeDEC(readClockByte(0xCD));
    AlarmM = makeDEC(readClockByte(0xCF));
  }
#endif
}

void setup() {
#ifdef SUPPORT_WATCHDOG
  wdt_reset();
  wdtDisable();
#endif
  Alarming = 0;
  Snoozing = 0;
  wakeTasks();
}

#ifdef SUPPORT_WATCHDOG
ISR(WDT_vect) {}                  //don't need to do anything here when the WDT wakes the MCU

//enable the wdt for 8sec interrupt
void wdtEnable(void)
{
    wdt_reset();
    cli();
    MCUSR = 0x00;
    WDTCR |= _BV(WDCE) | _BV(WDE);
    WDTCR = _BV(WDIE) | _BV(WDP3) | _BV(WDP0);    //8192ms
    sei();
}

//disable the wdt
void wdtDisable(void)
{
    wdt_reset();
    cli();
    MCUSR = 0x00;
    WDTCR |= _BV(WDCE) | _BV(WDE);
    WDTCR = 0x00;
    sei();
}
#endif

#ifdef SUPPORT_SLEEP
void goToSleep(void)
{
  bool keepSleeping = true;
  do {
    lcd.clear();
    lcd.noDisplay();
    setBacklight(LOW);
    setMainPower(HIGH); // Disable +5V

    DDRA = 0;
    DDRB = 0;
    PORTA = 0;
    PORTB = 0;

    //pinMode(PIN_MAINPOWER, INPUT);
    GIMSK |= _BV(PCIE0);                      //enable PCINT0
#ifdef SUPPORT_TILT
    GIMSK |= _BV(PCIE1);                      //enable PCINT1
    PCMSK0 = 0x01;                            // tilt-sensor
#endif
    PCMSK1 = 0x03;                            // buttons
    ACSRA |= _BV(ACD);                        //disable the analog comparator
    ADCSRA &= ~_BV(ADEN);                     //disable ADC
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();

#ifdef SUPPORT_WATCHDOG
    wdtEnable();
#endif
    sleep_cpu();                   //go to sleep
                                   //----zzzz----zzzz----zzzz----zzzz
    cli();                         //wake up here, disable interrupts
    GIMSK = 0x00;                  //disable INT0
    sleep_disable();

    wakeTasks();

#ifdef SUPPORT_WATCHDOG
    wdtDisable();
#endif
    sei();                         //enable interrupts again (but INT0 is disabled above)
    if (buttonWake) {              //what woke us
      keepSleeping = false;      //the button woke us, so turn the lights on again
      while (button0());
      while (button1());
    }
#ifdef SUPPORT_ALARM
    readClockTime();
    checkAlarm();
    if (Alarming) {
      keepSleeping = false;
    }
#endif
  } while (keepSleeping);
  
  buttonWake = false;
}

ISR(PCINT_vect)
{
    buttonWake = true;            //flag that the button woke us up (and not the wdt)
    GIMSK = 0x00;                 //disable INT0
}
#endif

bool button0() {
  bool val = digitalRead(PIN_BUTTON0);
  if(val) return false;
  while(!val) {
    delay(100);
    val = digitalRead(PIN_BUTTON0);
  }
  return true;
}

bool button1() {
  bool val = digitalRead(PIN_BUTTON1);
  if(val) return false;
  while(!val) {
    delay(100);
    val = digitalRead(PIN_BUTTON1);
  }
  return true;
}

#ifdef SUPPORT_TILT
bool tilt() {
  return digitalRead(PIN_TILT);
}
#endif

void printPadded(uint8_t val, char pad) {
  if(val < 10) lcd.print(pad);
  lcd.print(val);
}

void printTime(bool ampm) {
#ifdef SUPPORT_AMPM
  if(ampm) {
    uint8_t Hour12;
    if(Hour > 12) {
      Hour12 = Hour - 12;
    } else if(Hour == 0) {
      Hour12 = 12;
    } else {
      Hour12 = Hour;
    }
    printPadded(Hour12, ' ');
    lcd.print(':');
    printPadded(Min, '0');
    if(Hour > 11) {
      lcd.print(F(" PM"));
    } else {
      lcd.print(F(" AM"));
    }
  } else {
#endif
    printPadded(Hour, '0');
    lcd.print(':');
    printPadded(Min, '0');
    lcd.print(':');
    printPadded(Sec, '0');
#ifdef SUPPORT_AMPM
  }
#endif
}

#ifdef SUPPORT_ALARM
void printAlarm(bool ampm) {
#ifdef SUPPORT_AMPM
  if(ampm) {
    uint8_t Hour12;
    if(AlarmH > 12) {
      Hour12 = AlarmH - 12;
    } else if(AlarmH == 0) {
      Hour12 = 12;
    } else {
      Hour12 = AlarmH;
    }
    printPadded(Hour12, ' ');
    lcd.print(':');
    printPadded(AlarmM, '0');
    if(AlarmH > 11) {
      lcd.print(F(" PM"));
    } else {
      lcd.print(F(" AM"));
    }
  } else {
#endif
    printPadded(AlarmH, '0');
    lcd.print(':');
    printPadded(AlarmM, '0');
#ifdef SUPPORT_AMPM
  }
#endif
}
#endif

void printDate(bool longdate) {
#ifdef SUPPORT_LONGDATE
  if(longdate) {
    lcd.print(' ');
    switch(Month) {
      case 1: lcd.print(F("JAN")); break;
      case 2: lcd.print(F("FEB")); break;
      case 3: lcd.print(F("MAR")); break;
      case 4: lcd.print(F("APR")); break;
      case 5: lcd.print(F("MAY")); break;
      case 6: lcd.print(F("JUN")); break;
      case 7: lcd.print(F("JUL")); break;
      case 8: lcd.print(F("AUG")); break;
      case 9: lcd.print(F("SEP")); break;
      case 10: lcd.print(F("OCT")); break;
      case 11: lcd.print(F("NOV")); break;
      case 12: lcd.print(F("DEC")); break;
    }
    lcd.print(' ');
    printPadded(Date, ' ');
    lcd.print(' ');
  } else {
#endif
    printPadded(Month, ' ');
    lcd.print('/');
    printPadded(Date, ' ');
    lcd.print('/');
    printPadded(Year, '0');
#ifdef SUPPORT_LONGDATE
  }
#endif
}

#ifdef SUPPORT_ALARM
void dismiss() {
  AlarmD = Date;
  writeClockByte(0xC8, AlarmD);
  Alarming = 0;
  Snoozing = 0;
  writeClockByte(0xCB, 0);
}

#ifdef SUPPORT_SNOOZE
// FIXME: If you snooze across midnight then the snooze delay won't work as expected.
void snooze() {
  Snoozing = 1;
  Alarming = 0;
  AlarmH = Hour;
  AlarmM = Min + 10;
  if(AlarmM > 60) {
    AlarmH = (AlarmH + 1) % 24;
    AlarmM = AlarmM % 60;
  }
  writeClockByte(0xCA, 1);
  writeClockByte(0xCC, makeBCD(AlarmH));
  writeClockByte(0xCE, makeBCD(AlarmM));
}
#endif
#endif

unsigned long nextRefresh = 0;
unsigned long BacklightTimeout = 0;
unsigned long SleepTimeout = 0;
unsigned long AlarmTimeout = 0;
uint8_t State = 0, LastState = 255;

enum {
  STATE_DEFAULT=0,
  STATE_ASKSET_TIME,
  STATE_ASKSET_AMPM,
  STATE_ASKSET_DATE,
  STATE_ASKSET_LONG,
  STATE_ASKSET_ALARM,
  STATE_SETTIME_HOUR,
  STATE_SETTIME_MIN,
  STATE_SETDATE_MONTH,
  STATE_SETDATE_DATE,
  STATE_SETDATE_YEAR,
  STATE_SETALARM_HOUR,
  STATE_SETALARM_MIN,
  STATE_SETALARM_DATE,
  STATE_SETAMPM,
  STATE_SETLONG
};

#ifdef SUPPORT_ALARM
void checkAlarm() {
  if((Date != AlarmD) && ((Hour > AlarmH) || ((Hour == AlarmH) && (Min >= AlarmM)))) {
    Alarming = 1;
  }
}
#endif

void loop() {
  if(State != LastState) {
    lcd.clear();
    LastState = State;
#ifdef SUPPORT_SLEEP
    SleepTimeout = millis() + 15000;
#endif
  }

  switch(State) {
    case STATE_DEFAULT:
      if(Alarming) {
        if(millis() > AlarmTimeout) {
          for(int i = 150; i>0; i--) {
            int dly = Backlight ? 250-i : 150+i;
            setSpeaker(HIGH);
            delayMicroseconds(dly);
            setSpeaker(LOW);
            delayMicroseconds(dly);
          }
          setBacklight(!Backlight);
          BacklightTimeout = millis() + 5000;
          AlarmTimeout = millis() + 500;
        }
      }
      if(millis() > nextRefresh) {
        readClockTime();

#ifdef SUPPORT_ALARM
        checkAlarm();
        if(Alarming) {
#ifdef SUPPORT_SLEEP
          SleepTimeout = millis() + 15000;
#endif
        }
#endif

        lcd.clear();
        lcd.home();
        printTime(DisplayAMPM);
        lcd.setCursor(0, 1);
        printDate(DisplayLongDate);
        nextRefresh = millis() + 500;
      }

#ifdef SUPPORT_SLEEP
      if(millis() > SleepTimeout) {
        goToSleep();
        LastState = 255;
      }
#endif

      if(Backlight && (millis() > BacklightTimeout)) {
        setBacklight(LOW);
      }

#ifdef SUPPORT_TILT
      if(tilt()) {
          setBacklight(HIGH);
          BacklightTimeout = millis() + 5000;
      }
#endif

      if(button0()) {
#ifdef SUPPORT_SLEEP
        SleepTimeout = millis() + 15000;
#endif
#ifdef SUPPORT_ALARM
        if(Alarming) {
          dismiss();
        } else {
#endif
          setBacklight(!Backlight);
          BacklightTimeout = millis() + 5000;
#ifdef SUPPORT_ALARM
        }
#endif
      }
      if(button1()) {
#ifdef SUPPORT_SLEEP
        SleepTimeout = millis() + 15000;
#endif
#ifdef SUPPORT_SNOOZE
        if(Alarming) {
          snooze();
        } else {
#endif
          State = STATE_ASKSET_TIME;
#ifdef SUPPORT_SNOOZE
        }
#endif
      }
      break;
    case STATE_ASKSET_TIME:
      menuAsk(F("SetTime?"), STATE_SETTIME_HOUR, STATE_ASKSET_AMPM);
      break;
    case STATE_SETTIME_HOUR:
      lcd.home();
      printTime(DisplayAMPM);
      Hour = setVariable(0x84, F("Hour"), 0, 23, 1, STATE_SETTIME_MIN);
      break;
    case STATE_SETTIME_MIN:
      lcd.home();
      printTime(DisplayAMPM);
      Min = setVariable(0x82, F("Mins"), 0, 59, 1, STATE_DEFAULT);
      break;
    case STATE_ASKSET_AMPM:
#ifdef SUPPORT_AMPM
      menuAsk(F("SetAMPM?"), STATE_SETAMPM, STATE_ASKSET_DATE);
      break;
    case STATE_SETAMPM:
      writeClockByte(0xC0, !DisplayAMPM);
      DisplayAMPM = readClockByte(0xC1);
      State = STATE_DEFAULT;
      break;
#endif
    case STATE_ASKSET_DATE:
#ifdef SUPPORT_DATE
      menuAsk(F("SetDate?"), STATE_SETDATE_MONTH, STATE_ASKSET_LONG);
      break;
    case STATE_SETDATE_MONTH:
      lcd.home();
      printDate(0);
      Month = setVariable(0x88, F("Mon "), 1, 12, 1, STATE_SETDATE_DATE);
      break;
    case STATE_SETDATE_DATE:
      lcd.home();
      printDate(0);
      Date = setVariable(0x86, F("Date"), 1, 31, 1, STATE_SETDATE_YEAR);
      break;
    case STATE_SETDATE_YEAR:
      lcd.home();
      printDate(0);
      Year = setVariable(0x8C, F("Year"), 0, 99, 1, STATE_DEFAULT);
      break;
    case STATE_ASKSET_LONG:
#ifdef SUPPORT_LONGDATE
      menuAsk(F("SetLong?"), STATE_SETLONG, STATE_ASKSET_ALARM);
      break;
    case STATE_SETLONG:
      writeClockByte(0xC2, !DisplayLongDate);
      DisplayLongDate = readClockByte(0xC3);
      State = STATE_DEFAULT;
      break;
#endif
#endif
    case STATE_ASKSET_ALARM:
#ifdef SUPPORT_ALARM
      menuAsk(F("SetAlrm?"), STATE_SETALARM_HOUR, STATE_ASKSET_DATE);
      break;
    case STATE_SETALARM_HOUR:
      lcd.home();
      printAlarm(DisplayAMPM);
      AlarmH = setVariable(0xC4, F("Hour"), 0, 23, 1, STATE_SETALARM_MIN);
      break;
    case STATE_SETALARM_MIN:
      lcd.home();
      printAlarm(DisplayAMPM);
      AlarmM = setVariable(0xC6, F("Min "), 0, 59, 1, STATE_SETALARM_DATE);
      break;
    case STATE_SETALARM_DATE:
      writeClockByte(0xC8, 0);
#endif
      State = STATE_DEFAULT;
      break;
  }
}

void menuAsk(const __FlashStringHelper* label, uint8_t TrueState, uint8_t FalseState) {
  lcd.home();
  lcd.print(label);
  lcd.setCursor(0, 1);
  lcd.print(F("N      Y"));

  if(button0()) {
    State = TrueState;
  }
  if(button1()) {
    State = FalseState;
  }
}

uint8_t setVariable(uint8_t adr, const __FlashStringHelper* label, uint8_t mn, uint8_t mx, bool bcd, uint8_t NextState) {
  uint8_t val;

  val = readClockByte(adr);
  if(bcd) val = makeDEC(val);

  lcd.setCursor(0, 1);
  lcd.print(F("+ "));
  lcd.print(label);
  lcd.print(F(" >"));

  if(button1()) {
    val++;
    if(val > mx) val = mn;
    if(bcd) val = makeBCD(val);

    delay(10);
    writeClockByte(adr, val);
    delay(10);

    if(bcd) val = makeDEC(val);
  }
  if(button0()) {
    State = NextState;
  }

  return val;
}

void shiftOutDSClock(uint8_t val) {
  int i;
  pinMode(PIN_CLOCK_DAT, OUTPUT);

  for(i=0; i<=7; i++) {
    digitalWrite(PIN_CLOCK_DAT, bitRead(val, i));
    //digitalWrite(PIN_CLOCK_SCK, HIGH);
    PORTA |= 0x04;
    delayMicroseconds( 1);
    //digitalWrite(PIN_CLOCK_SCK, LOW);
    PORTA &= ~(0x04);
    delayMicroseconds( 1);
  }
}

uint8_t shiftInDSClock() {
  uint8_t val = 0;
  int i;
  pinMode(PIN_CLOCK_DAT, INPUT_PULLUP);

  for(i=0; i<=7; i++) {
    bitWrite(val, i, digitalRead(PIN_CLOCK_DAT));
    //digitalWrite(PIN_CLOCK_SCK, HIGH);
    PORTA |= 0x04;
    delayMicroseconds( 1);
    //digitalWrite(PIN_CLOCK_SCK, LOW);
    PORTA &= ~(0x04);
    delayMicroseconds( 1);
  }

  return val;
}

void startDSClock(void)
{
  //digitalWrite( PIN_CLOCK_SS, LOW); // default, not enabled
  //pinMode( PIN_CLOCK_SS, OUTPUT);

  //digitalWrite( PIN_CLOCK_SCK, LOW); // default, clock low
  //pinMode( PIN_CLOCK_SCK, OUTPUT);

  //pinMode( PIN_CLOCK_DAT, OUTPUT);

  PORTA &= ~(0x0e);
  DDRA |= 0x0e;

  //digitalWrite( PIN_CLOCK_SS, HIGH); // start the session
  PORTA |= 0x02;
  delayMicroseconds( 4);           // tCC = 4us
}

void stopDSClock(void)
{
  //digitalWrite( PIN_CLOCK_SS, LOW); // default, not enabled
  PORTA &= ~(0x02);
}

uint8_t readClockByte(uint8_t adr) {
  uint8_t val;

  adr |= 1; // Force Set Read bit

  startDSClock();
  shiftOutDSClock(adr);
  val = shiftInDSClock();
  stopDSClock();

  return val;
}

void writeClockByte(uint8_t adr, uint8_t val) {
  adr &= 0xfe; // Force Clear Read bit

  startDSClock();
  shiftOutDSClock(adr);
  shiftOutDSClock(val);
  stopDSClock();
}

uint8_t makeDEC(uint8_t val) {
  return ((val/16 * 10) + (val % 16));
}

uint8_t makeBCD(uint8_t val) {
  return ((val/10 * 16) + (val % 10));
}

void readClockTime() {
  startDSClock();

  shiftOutDSClock(0xBF);
  Sec = makeDEC(shiftInDSClock());
  Min = makeDEC(shiftInDSClock());
  Hour = makeDEC(shiftInDSClock());
  Date = makeDEC(shiftInDSClock());
  Month = makeDEC(shiftInDSClock());
  DayofWeek = makeDEC(shiftInDSClock());
  Year = makeDEC(shiftInDSClock());

  stopDSClock();
}

void writeClockTime() {
  startDSClock();

  shiftOutDSClock(0xBE);
  shiftOutDSClock(makeBCD(Sec));
  shiftOutDSClock(makeBCD(Min));
  shiftOutDSClock(makeBCD(Hour));
  shiftOutDSClock(makeBCD(Date));
  shiftOutDSClock(makeBCD(Month));
  shiftOutDSClock(DayofWeek);
  shiftOutDSClock(makeBCD(Year));

  stopDSClock();
}


