### Purchasing the Hardware ###

The Digital Watch Kit hardware can typically be purchased at various events including:

* [VCFse](https://atlhcs.org/)
* [Atlanta Maker Faire](http://atlanta.makerfaire.com/)

### Usage ###

* Install Arduino IDE
* Install ATTinyx61 Hardware Support
* Choose Features
* Build (Verify in Arduino IDE)
* Deploy

### Optional Features ###

The base firmware uses 3254 Bytes of AVR FLASH and 60 Bytes of AVR RAM
additional features will balloon this considerably.  If you wish to use
all the features then please upgrade to the ATTiny861 MCU.

* 12-Hour AM/PM mode

	Optionally display time as " 5:15 PM" instead of "17:15:33"
	( 236 Bytes AVR FLASH / 1 Byte AVR RAM / 1 Byte DS1302 NVRAM )

* Date Display

	( 154 Bytes AVR FLASH )

* Month Names

	Optinally display date as " Mar  5 " instead of " 5/05/17"

	( 312 Bytes AVR FLASH / 1 Byte AVR RAM / 1 Byte DS1302 NVRAM )

* Alarm Clock

	( 414-472 Bytes AVR FLASH / 4 Bytes AVR RAM / 3 Bytes DS1302 NVRAM )

* Power Down

	Powers down display and AVR after 15 seconds of inactivity.

	( 214-280 Bytes AVR FLASH / 5 Bytes AVR RAM )

* Watch Dog

	Wakes AVR every 8 seconds to check if alarm should go off.

	( 64 Bytes AVR Flash )

* Tilt Sensor

	Wakes AVR when you tilt your wrist for reading.  Install tilt sensor between AVR pins 5 and 20.

### Credits ###

* Flash Corliss - Design & BASCOM-AVR Programming 
* Alan Hightower - Board Layout
* David Kuder - This Arduino Firmware Programming